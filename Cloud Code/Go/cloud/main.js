
// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function (request, response) {
    response.success("Hello world!");
});


Parse.Cloud.job("notification", function (request, response) {
    Parse.Cloud.useMasterKey();

    var dateO = new Date();

    var query = new Parse.Query("Task");
    query.equalTo("done", false); // Remove this when replacing task.each()
    query.include("assignedTo");

    var day2 = 172800000;

    var count = 0;
    var notificationClass = Parse.Object.extend("Notification");
    var messageClass = Parse.Object.extend("Message");
    var notification;
    var date;
    var numT = 0;

    query.each(function (task) {
        // Check Done
        if (!task.get("notificationMade")) {
            date = task.get("taskDeadline").getTime();

            // Check year
            if (dateO >= (date - day2)) {

                notification = new notificationClass();

                if (task.get("assignedTo") != null) {
                    count++;
                    var myDate = task.get("taskDeadline");
                    var deadlineDate = myDate.getMonth() + "/" + myDate.getDate() + "/" + myDate.getFullYear();
                    var newACL = new Parse.ACL();
                    newACL.setWriteAccess(task.get("assignedTo"), true);
                    newACL.setReadAccess(task.get("assignedTo"), true);

                    //Set Notification Values
                    notification.setACL(newACL);
                    notification.set("taskPointer", task);
                    
                    notification.set("content", "Reminder:  '" + task.get("taskName") + "' is due " + deadlineDate);
                    notification.set('hasBeenRead', false);
                    task.set("notificationMade", true);
                    return notification.save();
                } else {
					count++;
                    var msg = new messageClass();
                    msg.setACL(task.getACL());
                    msg.set("message", "IMPORTANT: " + task.get("taskName") + " is due in 2 days!!");
                    msg.set("projectPointer", task.get("projectPointer"));
					
					task.set("notificationMade", true);
					task.save();
                    return msg.save();
                }
            };
        };

        return null;
    });

    var queryP = new Parse.Query("Project");

    var numP = 0;
    var numU = 0;

    // Find all Project
    queryP.find({
        success: function (results) {
            numP = results.length;

            // Find All users
            var userQuery = new Parse.Query(Parse.User);
            userQuery.each(function (user) {
                numU = numU + 1;
                var myComm = 0;
                var myHelp = 0;
                var myPart = 0;
                var counter = 0;

                // For every prject
                for (var i = 0; i < results.length; i++) {
                    var project = results[i];
                    var review = [];
                    review = project.get("peerReview");
                    if (review == null) {
                        continue;
                    }

                    // For every review
                    for (var j = 0; j < review.length; j++) {
                        if (review[j].ratee == user.id) {
                            myComm = myComm + review[j].communication;
                            myHelp = myHelp + review[j].helpfulness;
                            myPart = myPart + review[j].participation;
                            counter = counter + 1;
                        }
                    } // End of Every Review
                } // End of every project

                user.set("helpfulnessStat", myComm / counter);
                user.set("communicationStat", myHelp / counter);
                user.set("participationStat", myPart / counter);

                return user.save();
            }) // end of every User
                .then(function () {
                    // Set the job's success status
                    response.success("ExeTime = " + ((new Date()) - dateO) + ", Tasks notified " + count + ", Projects = " + numP + ", Users = " + numU);
                }, function (results, error) {
                    // Set the job's error status
                    response.error("Uh oh, something went wrong. error = " + error + " .... results = " + results);
                });
        },
        error: function (results, error) {
            response.error("Uh oh, something went wrong. error = " + error + " .... results = " + results);
        }
    });



    // #######################################
    // Replace Task.each query with this when ready to get task stat data
    // #######################################

    //var tasks = [];
    //queryP.find({
    //    success: function (results) {
    //        tasks = results;
    //        numP = results.length;

    //        // TODO: Gather stats on every task and place results per user into project array "taskStat"

    //        // for every task
    //        for (var i = 0; i < results.length; i++) {
    //            var t = results[i];

    //            // Check Done
    //            if (t.get("done")) {
    //                // Check Notification asready made
    //                if (!task.get("notificationMade")) {
    //                    date = task.get("taskDeadline").getTime();

    //                    // Check year
    //                    if ((date - day2) <= currentDate) {
    //                        if ((date - day1) >= currentDate) {

    //                            notification = new notificationClass();

    //                            // Check if its assigned
    //                            if (task.get("assignedTo") != null) {
    //                                count++;

    //                                var newACL = new Parse.ACL();
    //                                newACL.setWriteAccess(task.get("assignedTo"), true);
    //                                newACL.setReadAccess(task.get("assignedTo"), true);

    //                                //Set Notification Values
    //                                notification.setACL(newACL);
    //                                notification.set("taskPointer", task);
    //                                // notification.set("content", "wowcontent");

    //                                task.set("notificationMade", true);
    //                                notification.save();
    //                            }
    //                        };
    //                    };
    //                };
    //            }
    //        } // End of For Every Task

    //    }
    //});


});

