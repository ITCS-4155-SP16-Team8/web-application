Parse.Cloud.define("notification", function (request, response) {
    Parse.Cloud.useMasterKey();

    var query = new Parse.Query("Task");
    query.equalTo("done", false);

    var currentDate = new Date().getTime();

    var day1 = 86400000;
    var day2 = 172800000;

    var date;
    query.each(function (task) {

        // Check Done
        if (!task.get("notificationMade")) {
            date = task.get("taskDeadline").getTime();

            // Check year
            if ((date - day2) <= currentDate) {
                if ((date - day1) >= currentDate) {

                    var notification = new Parse.Object("Notification");

                    var newACL = new Parse.ACL();
                    newACL.setWriteAccess(task.get("assignedTo"), true);
                    newACL.setReadAccess(task.get("assignedTo"), true);

                    //Set Notification Values
                    notification.setACL(newACL);
                    notification.set("taskPointer", task);
                    //notification.set("content", task);

                    task.set("notificationMade", true);

                    notification.save();
                };
            };
        };
    }).then(function () {
        // Set the job's success status
        response.success("Notification completed successfully.");
    }, function (error) {
        // Set the job's error status
        response.error("Uh oh, something went wrong.");
    });
});