// JavaScript source code


function loadChart() {


    var query = new Parse.Query("Task");
    query.equalTo("projectPointer", Global_Project);

    query.find().then(function (tasks) {
        taskData = [5, 2, 3];
        //MEMBER TASK CHART
        var doughNutData = {
            labels: [
                "Wes",
                "Vince",
                "Kevin"
            ],
            datasets: [
                {
                    data: taskData,
                    backgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ]
                }]
        };

        var myDoughNutChart = new Chart($('#member-task-chart'), {
            type: 'doughnut',
            data: doughNutData,
            animation: {
                animateScale: true
            }
        });

        //TASK COMPLETION CHART
        var queryData = new Array(2);
        queryData[0] = 0;
        queryData[1] = 0;
        for (i = 0; i < tasks.length; i++) {
            if (tasks[i].get("done")) {
                queryData[0]++;
            }
            else {
                queryData[1]++;
            }
        }
        var doughNutData2 = {
            labels: [
                "Completed",
                "In Progress",
            ],
            datasets: [
                {
                    data: queryData,
                    backgroundColor: [
                        "#1FDE25",
                        "#939799"
                    ],
                    hoverBackgroundColor: [
                        "#36A2EB",
                        "#36A2EB"
                    ]
                }]
        };


        var myDoughNutChart2 = new Chart($('#task-completion-chart'), {
            type: 'doughnut',
            data: doughNutData2,
            animation: {
                animateScale: true
            }
        });
    }); //END OF QUERY
}