function RateTask() {
	$('#rate-task .error').empty();
    $('#rate-task .error').removeClass('alert alert-danger')
    $('#rate-task-complete').modal('toggle');
}

function confirmRateTask() {
	$('#rate-task-complete').modal('toggle');
	var value = $('#rate-task-slider').slider("option", "value");
	var ratingArray = []; 
	var rating = {
	    "rater": Parse.User.current().id,
		"rating": value
	};

	ratingArray = Current_Task.get("totalRatings");

	if(ratingArray == null || ratingArray.length == 0) {
		//if totalRatings has nothing, initialize it
		ratingArray = [rating];
		Current_Task.set("totalRatings", ratingArray);
	}
	else {
	    var evaluatedAlready = false;
	    var evaluationIndex;
		for(i = 0; i < ratingArray.length; i++) {
			if(ratingArray[i].rater == Parse.User.current().id) {
			    //if a user has already rated the task
			    evaluatedAlready = true;
			    evaluationIndex = i;
			    break;
			}
		}
	}

	if (!evaluatedAlready) {
	    ratingArray.push(rating);
	    Current_Task.set("totalRatings", ratingArray);
	    console.log("Pushed");
	}
	else {
	    ratingArray[evaluationIndex].rating = value;
	    Current_Task.set("totalRatings", ratingArray);
	}

	Current_Task.save({
		success: function() {
			loadProjectTasks();
		},
		error: function(error) {
			console.log("Failed.");
		}
	});
}

function changeRatingText(value) {
    id = $(value).selector;
    $(id + ' > span').popover({
        html: true,
        trigger: "manual",
        placement: "auto top",
        content: function() {
            switch(value.val()) {
                case "1": 
                    return "Terrible";
                    break;
                case "2":
                    return "Below Average";
                    break;
                case "3":
                    return "Average";
                    break;
                case "4":
                    return "Above Average";
                    break;
                case "5":
                    return "Excellent";
                    break;    
            }
        }
    });  
	$(id + ' > span').popover('show');
}