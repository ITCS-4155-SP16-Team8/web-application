/* Parse initialization and initial global object creation */

Parse.initialize("bWAeh72kpaBUWySXJDqDZBvGZE0YARSNiEDuxXYx", "V9uvpTEhxzhLNpkohyR3wpcrcC9eRF9TJahu2ew4");

var user = Parse.User.current();
var Global_Project = new Parse.Object("Project");
var Current_Task = new Parse.Object("Task");
var Data = [];
var rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="Mark Completed"></span></a>';
var byName = false;
var byRating = false;
var byDeadline = false;
var byAssignedTo = false;

$(document).ready(function () {

    /*  Determine Landing Page on Load */
    if (user == null) {  // If there is no user, send to main landing page to login/sign up
        $("#user-stuff").hide();
        $('#login').show();
        $("#aside").html(_.template($('#home-aside-view').html()));
        $("#section").html(_.template($('#home-section-view').html()));
    }
    else { //Otherwise, send logged in user to their user homepage
        redirect(); //This function is at the end of this file
    }

    /* Assign Datepicker to datepickers */
    $(".datepicker").datepicker();

    // Initialize Tooltips
    $('[data-toggle="tooltip"]').tooltip();

    //Initialize Sliders
    $('#rate-task-slider').slider({
        value: 3,
        min: 1,
        max: 5,
        step: 1,
        slide: function (event, ui) {
            $("#rate-task-slider").val(ui.value);
            changeRatingText($("#rate-task-slider"));
        }

    });


    $('#commstat').slider({
        value: 1,
        min: 1,
        max: 5,
        step: 1,
        slide: function (event, ui) {
            $("#commstat").val(ui.value);
            changeRatingText($("#commstat"));
        }
    });

    $('#helpstat').slider({
        value: 1,
        min: 1,
        max: 5,
        step: 1,
        slide: function (event, ui) {
            $("#helpstat").val(ui.value);
            changeRatingText($("#helpstat"));
        }
    });

    $('#partstat').slider({
        value: 1,
        min: 1,
        max: 5,
        step: 1,
        slide: function (event, ui) {
            $("#partstat").val(ui.value);
            changeRatingText($("#partstat"));
        }
    });



    // changeRatingText($("#amount"));

    /* 
    EVENT HANDLERS BELOW AND LOTS OF THEM 
    ----------------------------------------
    ----------------------------------------
    BEWARE, THIS IS WHY YOU USE A FRONTEND FRAMEWORK
    ----------------------------------------
    ----------------------------------------
    */

    // WH Dynamically attach an event to the add user button
    $(document).on('click', '.addUserBtn', function (e) {

        // WH call the addUser fn, passing in a jQuery object of the clicked button. 
        addUser($(this));
    });

    // LOAD PROJECT ON CLICK
    $(document).on('click', '.project-item', function (e) {
        loadProject($(this));
    });

    // LOAD ASSIGN TASK MODAL
    $(document).on('click', '.assign-user', function (e) {
        Current_Task.id = ($(this).data('tid'));
        assignUserModal($(this));
    });

    // ASSIGN TASK SUBMIT
    $(document).on('click', '.assignUserBtn', function (e) {
        assignUser($('#assignee-list option:selected').attr('value'));
    });

    // LOAD EDIT TASK MODAL
    $(document).on('click', '.edit-task', function (e) {
        Current_Task.id = ($(this).data('tid'));
        $("#edittask").trigger("reset");
        editTaskModal();
    });

    // DELETE TASK CONFIRMATION
    $(document).on('click', '.delete-task', function (e) {
        Current_Task.id = ($(this).data('tid'));
        areYouSure();
    });

    // DELETE TASK
    $(document).on('click', '#delete-task', function (e) {
        deleteTask();
    });

    // COMPLETE TASK CONFIRMATION
    $(document).on('click', '.complete-task', function (e) {
        Current_Task.id = ($(this).data('tid'));
        //determine whether people are rating tasks or completing the task
        if (Current_Task.get("assignedTo").id != Parse.User.current().id) {
            rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-resize-horizontal" data-toggle="tooltip" title="Rate the Task"></span></a>';
            RateTask();
        }
        else {
            rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="Mark Completed"></span></a>';
            confirmComplete();
        }
    });

    // COMPLETE TASK
    $(document).on('click', '#complete-task', function (e) {
        completeTask();
    });

    //COMPLETE PROJECT
    $(document).on('click', '#complete-project', function (e) {
        completeProject();
    });

    //CREATE PEER REVIEW
    $(document).on('click', '.peerReviewBtn', function (e) {
        peerReviewModal($(this));
    });

    // RATE TASK
    $(document).on('click', '#rate-task', function (e) {
        confirmRateTask();
    });
   
    // NOTIFICATION
    $(document).on('click', '#notification-pop', function (e) {
        getNotifications();
    });

    $(document).on('click', '#order-name', function(e) {
        sortByName();
    });

    $(document).on('click', '#order-rating', function(e) {
        sortByRating();
    });

    $(document).on('click', '#order-deadline', function(e) {
        sortByDeadline();
    });

    $(document).on('click', '#order-assignedTo', function(e) {
        sortByAssignedTo();
    });

    $(document).on('click', '#my-order-name', function(e) {
        mySortByName();
    });

    $(document).on('click', '#my-order-rating', function(e) {
        mySortByRating();
    });

    $(document).on('click', '#my-order-deadline', function(e) {
        mySortByDeadline();
    });

    $(document).on('click', '#my-order-projectname', function(e) {
        mySortByProjectName();
    });
    // DISMISS NOTIFICATION
    $(document).on('click', '.dismissNotification', function (e) {
        var notification = new Parse.Object("Notification");
        var note = ($(this).data('nid'));
        $('#' + note).hide();
        notification.id = note;
        notification.set("hasBeenRead", true);
        notification.save({
            success: function (notification) {
                console.log("Notification Saved");
                var query = new Parse.Query("Notification")
                query.equalTo('hasBeenRead', false);
                query.find({
                    success: function (notifications) {
                        $('#number-notifications').empty();
                        $('#number-notifications').append(notifications.length);
                    },
                    error: function (notifications, error) {
                        console.log("Error pulling notifications");
                    }
                });
            },
            error: function (notification, error) {
                console.log("Error saving notification");
            }
        });

    });
    

    //HACKY MODAL FIX - Doesn't fully work (only works on modal open)
    $('.modal').on('show.bs.modal', function () {
        if ($(document).height() > $(window).height()) {
            // no-scroll
            $('body').addClass("modal-open-scroll");
        }
        else {
            $('body').removeClass("modal-open-scroll");
        }
    })
    $('.modal').on('hide.bs.modal', function () {
        $('body').removeClass("modal-open-scroll");
    });

    //Local Storage for Task Filter Select
    // On refresh check if there are values selected
    if (localStorage.selectVal) {
        // Select the value stored
        $('#task-filter').val(localStorage.selectVal);
    }
    else {
        $('#task-filter').val("all");
    }

});

// On change store the value
$(document).on('change', '#task-filter', function (e) {
    var currentVal = $(this).val();
    localStorage.setItem('selectVal', currentVal);
    if(Global_Project.id == undefined) {
        console.log("kapap");
        loadUserTasks();
    }
    else {
        loadProjectTasks();
    }
});

function redirect() {

   
    $("#login").hide();
    $("#user-stuff").show();
    $("#profile").empty();
    $("#profile").prepend("<a href='' onclick='javascript: loadUserHome();'>"
        + "<img src='"
        + Parse.User.current().get("picture").url()
        + "'/>"
        + "<span id='husername'>"
        + Parse.User.current().get("username")
        + "</span>"
        + "</a>"
        );

    var query = new Parse.Query("Notification")
    query.equalTo('hasBeenRead', false);
    query.find({
        success:  function(notifications){
            $('#number-notifications').append(notifications.length);
        },
        error: function (notifications, error) {
            console.log("Error pulling notifications");
        }
    });

    $("#logout").append("<a id='logout-link' href='javascript: logOut();'>Logout</a>");
    $("#aside").html(_.template($('#project-list-view').html()));
    $("#section").html(_.template($('#userhome-view').html()));
    listProjects();
    loadUserHome();
}


$.fn.stars = function () {
    return this.each(function (i, e) { 
        $(e).html($('<span/>').width($(e).text() * 16)); });
};
