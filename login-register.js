function showRegisterForm(){
    $('.loginBox').fadeOut('fast',function(){
        $('.registerBox').fadeIn('fast');
        $('.login-footer').fadeOut('fast',function(){
            $('.register-footer').fadeIn('fast');
        });
        $('.modal-title').html('Register with');
    }); 
    $('.error').removeClass('alert alert-danger').html('');
       
}

function showLoginForm(){
    $('#register .registerBox').fadeOut('fast',function(){
        $('.loginBox').fadeIn('fast');
        $('.register-footer').fadeOut('fast',function(){
            $('.login-footer').fadeIn('fast');    
        });
        
        $('.modal-title').html('Login with');
    });       
     $('.error').removeClass('alert alert-danger').html(''); 
}

function openLoginModal(){
    showLoginForm();
    setTimeout(function(){
        $('#register').modal('show');    
    }, 230);
    
}

function openRegisterModal(){
    showRegisterForm();
    setTimeout(function(){
        $('#register').modal('show');    
    }, 230);
    
}

function loginSubmit(){
	var email = $('#email').val();
	var pass = $('#password').val();
	
	Parse.User.logIn(email, pass, {
		success: function(user) {
			redirect();
		},
		error:  function(user, error) {
			//Login Failure
			loginError();
		}
	});	
}

function registerSubmit(){
    var upload = $('#rfile')[0];
    var email = $('#remail').val();
    var username = $('#rusername').val();
	var pass = $('#rpassword').val();
	var passv = $('#rpassword_confirmation').val();
	var picture = new Parse.File("photo.jpg", upload.files[0]);

	if (pass == passv){
	    var user = new Parse.User();  
		user.set("username", username);
		user.set("email", email);
		user.set("password", pass);
		user.set("picture", picture);
		console.log("Username - " + username);
		console.log("Email - " + email);
		
		user.signUp(null, {
		    success: function (user) {
		        $('#register').modal('toggle');
		        redirect();
		    },
		    error: function (user, error) {
		        // Show the error message somewhere and let the user try again.
		        alert("Error: " + error.code + " " + error.message);
		    }
		});
	}
	else {
		registrationError();
		//TODO - Include a modal line that indicates passwords do not match
	}
}

function loginError(){
    $('#register .modal-dialog').addClass('shake');
             $('.error').addClass('alert alert-danger').html("Invalid email/password combination");
             $('input[type="password"]').val('');
             setTimeout( function(){ 
                $('#lregister .modal-dialog').removeClass('shake'); 
    }, 1000 ); 
}

function registrationError(){
	$('#register .modal-dialog').addClass('shake');
             $('.error').addClass('alert alert-danger').html("Passwords do not match");
             $('input[type="password"]').val('');
             setTimeout( function(){ 
                $('#lregister .modal-dialog').removeClass('shake'); 
    }, 1000 ); 
}

function logOut(){
	Parse.User.logOut();
	window.location.replace("./index.html");
}	
	


   