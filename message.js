function loadMessages(id) {
    event.preventDefault();
    
    $("#section").html(_.template($('#project-message-view').html()));
    
    var query = new Parse.Query("Message");
    query.limit(1000);
    query.equalTo("projectPointer", Global_Project);
    query.find({
        success: function (results) {

            for (var i = 0; i < results.length; i++) {
                date = results[i].get("createdAt");
                var hour;
                var ampm = "AM";
                if (date.getUTCHours() > 12) {
                    hour = date.getUTCHours() - 12;
                    ampm = "PM";
                }
                var minutes = ('0' + date.getUTCMinutes()).slice(-2);
                $('#message-center')
                    .append($('<div>')
                        .addClass('messageContainer')
                        .append(
                            $('<h3>')
                                .html(results[i].get("postBy").get("username")))
                        .append(
                            $('<p>')
                                .html(results[i].get("message"))
                        )
                        .append(
                            $('<p>')
                                .addClass('timeStamp')
                                .html(date.getMonth()+'/'+date.getDate()+'/'+date.getFullYear() + "  " + hour + ":" + minutes + " " + ampm)
                        )
                    );
            }
            $("#message-center").animate({ scrollTop: $('#message-center').prop("scrollHeight") }, 1);
        },
        error: function (error) {
            alert("Error: " + error.code + " " + error.message);
        }
    });

}

function createMessage() {
    event.preventDefault();

    var messageToPost = $('#message').val();

    var message = new Parse.Object("Message");
    message.set("message", messageToPost);
    message.set("postBy", Parse.User.current());
    message.set("projectPointer", Global_Project);
    message.setACL(Global_Project.getACL());
    message.save(null, {
        success: function (message) {
            loadMessages();
        },
        error: function (message, error) {
            console.log("ERROR!  " + error);
        }
    });
}