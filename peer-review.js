function getPeerReview() {
    var obj = Global_Project.get("peerReview");
    ratings = obj[Parse.User.current().id];
    //console.log(ratings["TDDSDzGMNB"][0]);
}

function peerReviewModal($btn) {
    $("#userReview").empty();
    $("#userReview").html($btn.data('un'));
    $("#PRsubmit").attr('data-uid', $btn.data('uid'));
    $("#create-peer-review").modal('toggle');
}

function createPeerReview() {
    var commStat = $('#commstat').slider("option", "value");
    var helpStat = $('#helpstat').slider("option", "value");
    var partStat = $('#partstat').slider("option", "value");
    var evaluatee = $('#PRsubmit').attr('data-uid');
    var evaluator = Parse.User.current().id;

    var theArray = []
    theArray = Global_Project.get("peerReview");

    var newObj = {
        "ratee": evaluatee,
        "rater": Parse.User.current().id,
        "communication": commStat,
        "helpfulness": helpStat,
        "participation": partStat
    }
    
    if (theArray == null || theArray.length == 0) {
        console.log("Shits empty dawg");
        var a = [];
        a.push(newObj);
        Global_Project.set("peerReview", a);
        Global_Project.save({
            success: function (project) {
                console.log("Saved New Array!");
            },
            error: function (project, error) {
                console.log("Error Saving New Array!");
            }
        });
    }

    else {
        var evaluatedAlready = false;
        for (i in theArray) {
            if (theArray[i].rater == Parse.User.current().id && theArray[i].ratee == evaluatee) {
                console.log("I have already rated this person");
                evaluatedAlready = true;
                evaluationIndex = i;
                break;
            }
        }
        if (!evaluatedAlready) {
            console.log("I have not rated this person");
            theArray.push(newObj);
            Global_Project.set("peerReview", theArray);
            Global_Project.save({
                success: function (project) {
                    console.log("Saved Data to the Array!");

                },
                error: function (project, error) {
                    console.log("Error Saving New Array!");
                }
            });
        }
        else {
            console.log("Rating Updated");
            theArray[i] = newObj;
            Global_Project.set("peerReview", theArray);
            Global_Project.save({
                success: function (project) {
                    console.log("Saved Data to the Array!");
                },
                error: function (project, error) {
                    console.log("Error Saving New Array!");
                }
            });
        }
    }
    $("#create-peer-review").modal('toggle');
    loadOverview(Global_Project.id);
}

function getUserStats(id){
    var statsArray = [];
    var counter = 0;
    var commTotal = 0;
    var helpTotal = 0;
    var partTotal = 0;
    var finalStats = [];

    statsArray = Global_Project.get("peerReview");

    if (statsArray != null) {
        for (var i = 0; i < statsArray.length; i++) {
            if (statsArray[i].ratee == id) {
                commTotal = commTotal + statsArray[i].communication;
                helpTotal = helpTotal + statsArray[i].helpfulness;
                partTotal = partTotal + statsArray[i].participation;
                counter++;
            }
        }
        finalStats = [commTotal / counter, helpTotal / counter, partTotal / counter];
        
    }

    return finalStats;

}

