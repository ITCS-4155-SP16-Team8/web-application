/* PROJECT STUFF */

function listProjects() {
    $("#project-list").empty();

    var user = Parse.User.current();
    var query = new Parse.Query("Project");

    query.find({
        success: function (results) {
            for (var i = 0; i < results.length; i++) {
                var project = results[i];
                var name = project.get("projectName");
                var id = project.id;

                if (results[i].get("done")) {
                    $('#project-list')
                        .append(
                        $('<li>')
                            .addClass("done")
                            .append(
                         $('<a>')
                            .attr("data-pid", id)
                            .addClass("project-item")
                            .append("<span/>")
                            .text(name)));
                }
                else {
                    $('#project-list').append(
                        $('<li>')
                        .append(
                            $('<a>')
                            .attr("data-pid", id)
                            .addClass("project-item")
                            .append("<span/>")
                                .text(name)));
                }
            }
        }
    });
}

function loadProject($id) {
    var id = $id.data('pid');
    var query = new Parse.Query(Global_Project);
    query.include("projectRole");
    query.equalTo("objectId", id);
    query.find({
        success: function (results) {
            Global_Project = results[0];
            $('li').removeClass("highlighted");
            $($id).parent().addClass("highlighted");
            loadOverview(Global_Project.id);
        },
        error: function (results, error) {
            console.log("Error!  " + error.code + " : " + error.message);
        }
    });
}

function loadOverview(id) {
    event.preventDefault();
    $("#section").html(_.template($('#project-overview').html()));
    $('#member-list').empty();
    $("#project-name").html(Global_Project.get("projectName"));
    var description = Global_Project.get("projectDescription");
    var deadline = Global_Project.get("projectDeadline");
    //var reviewObj = Global_Project.get("peerReview");
    $('#project-description').html("Project Description:").append(
        $('<p>').html(description));
    $('#deadline').html("Due Date:").append(
        $('<p>').html(deadline.getMonth() + "/" + deadline.getDate() + "/" + deadline.getFullYear()));
    $('#member-list').append(
            $('<tr>').append(
                $('<td>').html("Member"))
            .append(
                $('<td>').html("Communication"))
            .append(
                $('<td>').html("Helpfulness"))
            .append(
                $('<td>').html("Participation"))
            .append(
                $('<td>').html(""))
    );

    function createRows(results) {
        for (var i = 0; i < results.length; i++) {

            var userStats = [];
            var userStats = getUserStats(results[i].id);

            //If User has no ratings for this project
            if (userStats.length == 0 || isNaN(userStats[0])) {
                userStats = [0, 0, 0];
                $('#member-list').append(
                $("<tr>").append(
                    $("<td>").html(results[i].get("username")))
                .append(
                    $("<td>")
                        .append(
                            $("<span>")
                                .addClass('stars')
                                .html(0)))
                .append(
                    $("<td>")
                        .append(
                            $("<span>")
                                .addClass('stars')
                                .html(0)))
                .append(
                    $("<td>")
                        .append(
                            $("<span>")
                                .addClass('stars')
                                .html(0)))
                .append(
                    $("<td>"))
                );
            }

                //If the user is me!
            else if (Parse.User.current().id == results[i].id) {
                $('#member-list').append(
                    $("<tr>").append(
                        $("<td>").html(results[i].get("username")))
                    .append(
                        $("<td>")
                        .append(
                            $("<span>")
                                .addClass('stars')
                                .html(userStats[0])))
                    .append(
                        $("<td>")
                        .append(
                            $("<span>")
                                .addClass('stars')
                                .html(userStats[1])))
                    .append(
                        $("<td>")
                        .append(
                            $("<span>")
                                .addClass('stars')
                                .html(userStats[2])))
                    .append(
                        $("<td>"))
                    );

            }

                //If the user is anybody else with ratings for this project
            else {
                $('#member-list').append(
                    $("<tr>").append(
                        $("<td>").html(results[i].get("username")))
                    .append(
                        $("<td>")
                            .append(
                                $("<span>")
                                    .addClass('stars')
                                    .html(userStats[0])))
                    .append(
                        $("<td>")
                            .append(
                                $("<span>")
                                    .addClass('stars')
                                    .html(userStats[1])))
                    .append(
                        $("<td>")
                            .append(
                                $("<span>")
                                    .addClass('stars')
                                    .html(userStats[2])))
                    .append(
                        $("<td>").append(
                            $('<button>')
                                .attr('type', "button")
                                .addClass('btn btn-success btn-sm')
                                .addClass('peerReviewBtn')
                                .attr('data-un', results[i].get("username"))
                                .attr('data-uid', results[i].id)
                                .html('Rate This Person')))
                    );
            }
        }
    }


    var Role = new Parse.Role();
    Role = Global_Project.get("projectRole");
    var relation = Role.getUsers();

    //GET ALL USERS IN PROJECT
    var query = relation.query();
    query.ascending("username");
    query.find().then(function (users) {
        return users;
    }).then(function (users) {
        return createRows(users);
    }).then(function(rows){
        $('.stars').stars();
    });


    // CHART STUFF

    var startDate = Global_Project.get("createdAt");
    var endDate = Global_Project.get("projectDeadline");
    var query = new Parse.Query("Task");
    Data = [];
    query.equalTo("projectPointer", Global_Project);
    query.find({
        success: function (results) {
            for (var i = 0; i < results.length; i++) {
                Data.push({
                    //'label': results[i].get("taskName"),
                    'times': [{
                        "label": results[i].get("taskName"),
                        "starting_time": results[i].get("createdAt").getTime(),
                        "ending_time": results[i].get("updatedAt").getTime()
                    }]
                });
            }
            loadChart();
        },
        error: function (results, error) {

        }
    });
}


function createProject() {

    //Create ACL
    var newACL = new Parse.ACL();
    newACL.setWriteAccess(Parse.User.current(), true);
    newACL.setReadAccess(Parse.User.current(), true);

    //Create Role
    var newRole = new Parse.Role(guid(), newACL);
    newRole.getUsers().add(Parse.User.current());

    //Save Role
    newRole.save(null, {
        success: function (newRole) {
            //Successful Save of Role
            //After Role is Saved, take its return and create a Project
            var Project = new Parse.Object("Project");
            var newACL2 = new Parse.ACL();
            newACL2.setRoleReadAccess(newRole, true);
            newACL2.setRoleWriteAccess(newRole, true);
            Project.set("projectName", $("#projectname").val());
            Project.set("projectDescription", $("#description").val());
            Project.set("projectDeadline", $("#project-deadline").datepicker("getDate"));
            Project.set("projectRole", newRole);
            Project.setACL(newACL2);

            //Now we save the Project
            Project.save(null, {
                success: function (project) {
                    //Role was saved, Project was saved, now we finish!
                    //reset the form, collapse it, reload project List
                    //Add message that project was created, will be first project message
                    $("#createproject").trigger("reset");
                    $('#create-project').modal('toggle');
                    var message = new Parse.Object("Message");
                    message.set("message", project.get("projectName") + " project has been created.");
                    message.set("postBy", Parse.User.current());
                    message.set("projectPointer", project);
                    message.setACL(project.getACL());
                    message.save(null, {
                        success: function (message) {
                            listProjects();
                        },
                        error: function (message, error) {
                            console.log("ERROR!  " + error);
                        }
                    });

                },
                error: function (project, error) {
                    //Failed to Save Project
                    console.log("Error!" + error);
                }
            });
        },
        error: function (newRole, error) {
            console.log("Error Saving Role:  " + error.code + " : " + error.message);
        }
    });
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x1000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function searchUser() {

    $('#result-table').empty();
    var target = $("#user-name").val();
    var User = new Parse.User();
    var query = new Parse.Query(User);
    query.contains("username", target);
    query.find().then(function (results) {
        for (i in results) {
            var username = results[i].get("username");
            var email = results[i].get("email");
            var userid = results[i].id;
            var userImage = results[i].get("picture");
            console.log(userImage);
            if (userImage == null) {
                image = "personIcon.png";
            }
            else {
                image = userImage.url();
            }
            var userCommunication = results[i].get("communicationStat");
            var userHelpfulness = results[i].get("helpfulnessStat");
            var userParticipation = results[i].get("participationStat");
            if (userParticipation == null) {
                userParticipation = 0;
                userCommunication = 0;
                userHelpfulness = 0;
            }

            $("#result-table").append(
                //New Row
                $("<tr>")
                    .append(
                        //User Image
                        $('<td>')
                            .attr("rowspan", 3)
                            .append("<img class='userImage' src='" + image + "'/>")
                        )
                    .append(
                        //User Name
                        $('<td>')
                            .append(username)
                        )
                    .append(
                        //Participation Stat
                        $('<td>')
                            .append(
                                $('<span>')
                                    .addClass('stars')
                                    .html(userCommunication)
                            )
                    )
                    .append(
                        //Add Button
                        $('<td>')
                            .attr("colspan", 3)
                            .append(
                                $('<button>')
                                    .attr('type', "button")
                                    .addClass('btn btn-primary btn-sm')
                                    .addClass('addUserBtn')
                                    .attr('data-uid', userid)
                                    .html('ADD THIS PERSON!')
                                )
                    ))

                //ROW 2
                .append(
                $('<tr>')
                    //User Email
                    .append(
                        $('<td>')
                            .append(email)
                        )
                    .append(
                        $('<td>').append(
                            $('<span>')
                                .addClass('stars')
                                .html(userHelpfulness)
                    ))
                )

                //ROW 3
                .append(
                $('<tr>')
                    //User Email
                    .append(
                        $('<td>')
                            .append("")
                        )
                    .append(
                        $('<td>')
                            .append(
                            $('<span>')
                                .addClass('stars')
                                .html(userParticipation)
                    ))
                );



            /* WH basically using the data attribute as a means of storing the UID. The addUserBtn class on this button is the
           * hook for the the event listener we created at the top of this page.  
            addUserBtn*/
            //Button Creation had to be heavily modified as it was inserting a space before the userID
        }
    }).then(function (result) {
        $('.stars').stars();
    });




}

function addUser($btn) {
    // WH $btn is a jQuery object of the addUserBtn. This allows us to use .data(): http://api.jquery.com/data/
    var userid = $btn.data('uid');
    var Role = new Parse.Role();
    Role = Global_Project.get("projectRole");
    var ACL = Role.getACL();
    var query = new Parse.Query(Parse.User);
    query.equalTo("objectId", userid);
    query.find({
        success: function (results) {
            Role.getUsers().add(results[0]);
            ACL.setReadAccess(results[0], true);
            ACL.setWriteAccess(results[0], true);
            Role.setACL(ACL);
            Role.save(null, {
                success: function (newRole) {
                    $('#add-user').modal('toggle');
                    loadOverview(Global_Project.id);
                },
                error: function (error) {
                    console.log("Error saving role!")
                }
            });

        },
        error: function (error) {
            console.log(error);
        }
    });
}

function completeProject() {

    Global_Project.set("done", true);
    Global_Project.save({
        success: function (Project) {
            $('#confirm-complete-p').modal('toggle');
            listProjects();
        },
        error: function (Project, error) {
            console.log("Error Saving Project after Completion:  " + error);
        }
    });
}