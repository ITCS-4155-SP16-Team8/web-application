/* USER FUNCTIONS */

function loadUserHome() {
    event.preventDefault();
    $("#project-list").children().removeClass("highlighted");
    $("#section").html(_.template($('#userhome-view').html()));
    $('#userheader').append(Parse.User.current().get("username"));
}