function sortByName() {
	event.preventDefault();
	$("#section").html(_.template($('#project-task-view').html()));
	var query = new Parse.Query("Task");

	byName = !byName;

	$('#task-filter').val(localStorage.selectVal);

    switch (localStorage.selectVal) {
        case "all":
            var query = new Parse.Query("Task");
            break;
        case "inprogress":
            query.equalTo("done", false);
            break;
        case "completed":
            query.equalTo("done", true);
            break;
        case "overdue":
            query.equalTo("done", false);
            query.lessThan("taskDeadline", date);
            break;
        case "mine":
            query.equalTo("assignedTo", Parse.User.current());
            break;
    }

    if(byName) {
		query.ascending("taskName");
	}
	else {
		query.descending("taskName");
	}

    query.equalTo("projectPointer", Global_Project);

	query.find({
        //for collapsible task view
        success: function (results) {

            for (var i = 0; i < results.length; i++) {
                var task = results[i];
                var name = task.get("taskName");
                var id = task.id;
                var description = task.get("taskDescription");
                var deadline = task.get("taskDeadline");
                var assigned = new Parse.User();
                assigned = task.get("assignedTo");
                ratings = task.get("totalRatings");
                var totalRatings = 0;

                if (ratings == null) {
                    totalRatings = 0;
                }
                else {
                    for (var j = 0; j < ratings.length; j++) {
                        totalRatings = (totalRatings + ratings[j].rating);
                    }
                    totalRatings = totalRatings / ratings.length;
                    totalRatings = totalRatings.toFixed(2);
                }

                if (task.get("assignedTo") != null) {
                    var assignedUser = assigned.get("username");
                }
                else {
                    var assignedUser = "Unassigned";
                }

                //To change between the rate task button and mark complete button
                if(assignedUser != Parse.User.current().get("username")) {
                    rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-stats" data-toggle="tooltip" title="Rating Tasks"></span></a>';
                }
                else {
                    rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="Mark Completed"></span></a>';
                }

                //Select the blank table built into the HTML
                $('#tasks').append(

                //CREATE ITEM[i] HEADER
                $('<tr>')
                .addClass('panel panel-default clickable hr') //hr class is super important in css
                .attr("data-target", id)
                .attr('data-toggle', 'collapse')
                .attr("id", "accordion" + id)
                .append(
                    //TASK TITLE
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<h4>')
                            .append(
                                    $('<a>')
                                    .attr("data-toggle", "collapse")
                                    .attr("data-parent", "#accordion" + id)
                                    .attr('href', '#' + id)
                                    .append(name))))
                .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<span>')
                            .addClass('stars')
                            .html(totalRatings)))
                .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<h4>')
                            .html((deadline.getMonth() + 1) + "/" + deadline.getDate() + "/" + deadline.getFullYear())))
                .append(
                    //SHOW ASSIGNED USER
                    $('<td>')
                            .addClass('panel panel-heading')
                            .append(
                                $('<h4>')
                                    .html(assignedUser)))

                .append(
                    //CREATE BUTTONS 
                    $('<td>')
                            .addClass('panel panel-heading block')
                            .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-danger delete-task" data-toggle="tooltip" title="Delete Task"><span class="glyphicon glyphicon-trash"></span></a>'))
                            .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-info edit-task"><span class="glyphicon glyphicon-pencil" data-toggle="tooltip" title="Edit Task"></span></a>'))
                            .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm assign-user"><span class="glyphicon glyphicon-user" data-toggle="tooltip" title="Assign User"></span></a>'))
                            .append($('<a href="#" data-tid="' + id + rateButton))
                    ));

                //CREATE ITEM[i] COLLAPSIBLE CONTENT
                $('#tasks').append(
                    $('<tr>')
                    .attr("id", id)
                    .addClass('panel-collapse collapse')
                    .append(
                    $('<td>')
                        .attr("colspan", 4)
                        .addClass('panel panel-body')
                        .append(
                            $('<p>')
                            .addClass('item-header')
                            .append("Description:"))
                            .append(
                            $('<p>')
                            .append(description))));


                //Check if Task is Done
                if (results[i].get("done")) {
                    if (i == 0) {
                        $('#tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                    else {
                        $('#tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                }

                else {
                    //Check if Task is Overdue

                    if (deadline < date) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                        else {
                            $('#tasks tr:eq(' + (i * 2) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                    }

                    //Check if Task is due soon (within 2 days)
                    var date2 = new Date(date).setDate(date.getDate() + 2);
                    if (deadline > date && deadline < date2) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                        else {
                            $('#tasks tr:eq(' + (i * 2) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                    }
                }
                //Check if Task is Done
                if (results[i].get("done")) {
                    if (i == 0) {
                        $('#tasks tr:eq(' + (i + 1) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                    else {
                        $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                }

                else {
                    //Check if Task is Overdue

                    if (deadline < date) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                        else {
                            $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                    }

                    //Check if Task is due soon (within 2 days)
                    var date2 = new Date(date).setDate(date.getDate() + 2);
                    if (deadline > date && deadline < date2) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                        else {
                            $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                    }
                }
            }// END FOR LOOP
		}
	}).then(function (rows) {
	    $('.stars').stars();
	});
}

function sortByRating() {
	event.preventDefault();
	$("#section").html(_.template($('#project-task-view').html()));
	var query = new Parse.Query("Task");

	byRating = !byRating;

	switch (localStorage.selectVal) {
        case "all":
            var query = new Parse.Query("Task");
            break;
        case "inprogress":
            query.equalTo("done", false);
            break;
        case "completed":
            query.equalTo("done", true);
            break;
        case "overdue":
            query.equalTo("done", false);
            query.lessThan("taskDeadline", date);
            break;
        case "mine":
            query.equalTo("assignedTo", Parse.User.current());
            break;
    }

    if(byRating) {
		query.ascending("totalRatings");
	}
	else {
		query.descending("totalRatings");
	}

	if(Global_Project.id == null) {
    	query.equalTo("assignedTo", user);
    }
    else {
    	query.equalTo("projectPointer", Global_Project);
    }

    query.equalTo("projectPointer", Global_Project);
	query.find({
        //for collapsible task view
        success: function (results) {

            for (var i = 0; i < results.length; i++) {
                var task = results[i];
                var name = task.get("taskName");
                var id = task.id;
                var description = task.get("taskDescription");
                var deadline = task.get("taskDeadline");
                var assigned = new Parse.User();
                assigned = task.get("assignedTo");
                ratings = task.get("totalRatings");
                var totalRatings = 0;

                if (ratings == null) {
                    totalRatings = 0;
                }
                else {
                    for (var j = 0; j < ratings.length; j++) {
                        totalRatings = (totalRatings + ratings[j].rating);
                    }
                    totalRatings = totalRatings / ratings.length;
                    totalRatings = totalRatings.toFixed(2);
                }

                if (task.get("assignedTo") != null) {
                    var assignedUser = assigned.get("username");
                }
                else {
                    var assignedUser = "Unassigned";
                }

                //To change between the rate task button and mark complete button
                if(assignedUser != Parse.User.current().get("username")) {
                    rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-stats" data-toggle="tooltip" title="Rating Tasks"></span></a>';
                }
                else {
                    rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="Mark Completed"></span></a>';
                }

                //Select the blank table built into the HTML
                $('#tasks').append(

                //CREATE ITEM[i] HEADER
                $('<tr>')
                .addClass('panel panel-default clickable hr') //hr class is super important in css
                .attr("data-target", id)
                .attr('data-toggle', 'collapse')
                .attr("id", "accordion" + id)
                .append(
                    //TASK TITLE
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<h4>')
                            .append(
                                    $('<a>')
                                    .attr("data-toggle", "collapse")
                                    .attr("data-parent", "#accordion" + id)
                                    .attr('href', '#' + id)
                                    .append(name))))
                .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<span>')
                            .addClass('stars')
                            .html(totalRatings)))
                .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<h4>')
                            .html((deadline.getMonth() + 1) + "/" + deadline.getDate() + "/" + deadline.getFullYear())))
                .append(
                    //SHOW ASSIGNED USER
                    $('<td>')
                            .addClass('panel panel-heading')
                            .append(
                                $('<h4>')
                                    .html(assignedUser)))

                .append(
                    //CREATE BUTTONS 
                    $('<td>')
                            .addClass('panel panel-heading block')
                            .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-danger delete-task" data-toggle="tooltip" title="Delete Task"><span class="glyphicon glyphicon-trash"></span></a>'))
                            .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-info edit-task"><span class="glyphicon glyphicon-pencil" data-toggle="tooltip" title="Edit Task"></span></a>'))
                            .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm assign-user"><span class="glyphicon glyphicon-user" data-toggle="tooltip" title="Assign User"></span></a>'))
                            .append($('<a href="#" data-tid="' + id + rateButton))
                    ));

                //CREATE ITEM[i] COLLAPSIBLE CONTENT
                $('#tasks').append(
                    $('<tr>')
                    .attr("id", id)
                    .addClass('panel-collapse collapse')
                    .append(
                    $('<td>')
                        .attr("colspan", 4)
                        .addClass('panel panel-body')
                        .append(
                            $('<p>')
                            .addClass('item-header')
                            .append("Description:"))
                            .append(
                            $('<p>')
                            .append(description))));


                //Check if Task is Done
                if (results[i].get("done")) {
                    if (i == 0) {
                        $('#tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                    else {
                        $('#tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                }

                else {
                    //Check if Task is Overdue

                    if (deadline < date) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                        else {
                            $('#tasks tr:eq(' + (i * 2) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                    }

                    //Check if Task is due soon (within 2 days)
                    var date2 = new Date(date).setDate(date.getDate() + 2);
                    if (deadline > date && deadline < date2) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                        else {
                            $('#tasks tr:eq(' + (i * 2) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                    }
                }
                //Check if Task is Done
                if (results[i].get("done")) {
                    if (i == 0) {
                        $('#tasks tr:eq(' + (i + 1) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                    else {
                        $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                }

                else {
                    //Check if Task is Overdue

                    if (deadline < date) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                        else {
                            $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                    }

                    //Check if Task is due soon (within 2 days)
                    var date2 = new Date(date).setDate(date.getDate() + 2);
                    if (deadline > date && deadline < date2) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                        else {
                            $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                    }
                }
            }// END FOR LOOP
		}
	}).then(function (rows) {
	    $('.stars').stars();
	});
}

function sortByDeadline() {
	event.preventDefault();
	$("#section").html(_.template($('#project-task-view').html()));
	var query = new Parse.Query("Task");

	byDeadline = !byDeadline;

	switch (localStorage.selectVal) {
        case "all":
            var query = new Parse.Query("Task");
            break;
        case "inprogress":
            query.equalTo("done", false);
            break;
        case "completed":
            query.equalTo("done", true);
            break;
        case "overdue":
            query.equalTo("done", false);
            query.lessThan("taskDeadline", date);
            break;
        case "mine":
            query.equalTo("assignedTo", Parse.User.current());
            break;
    }

    if(byDeadline) {
		query.ascending("taskDeadline");
	}
	else {
		query.descending("taskDeadline");
	}

	if(Global_Project.id == null) {
    	query.equalTo("assignedTo", user);
    	console.log("Hi");
    }
    else {
    	query.equalTo("projectPointer", Global_Project);
    }

    query.equalTo("projectPointer", Global_Project);
	query.find({
        //for collapsible task view
        success: function (results) {

            for (var i = 0; i < results.length; i++) {
                var task = results[i];
                var name = task.get("taskName");
                var id = task.id;
                var description = task.get("taskDescription");
                var deadline = task.get("taskDeadline");
                var assigned = new Parse.User();
                assigned = task.get("assignedTo");
                ratings = task.get("totalRatings");
                var totalRatings = 0;

                if (ratings == null) {
                    totalRatings = 0;
                }
                else {
                    for (var j = 0; j < ratings.length; j++) {
                        totalRatings = (totalRatings + ratings[j].rating);
                    }
                    totalRatings = totalRatings / ratings.length;
                    totalRatings = totalRatings.toFixed(2);
                }

                if (task.get("assignedTo") != null) {
                    var assignedUser = assigned.get("username");
                }
                else {
                    var assignedUser = "Unassigned";
                }

                //To change between the rate task button and mark complete button
                if(assignedUser != Parse.User.current().get("username")) {
                    rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-stats" data-toggle="tooltip" title="Rating Tasks"></span></a>';
                }
                else {
                    rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="Mark Completed"></span></a>';
                }

                //Select the blank table built into the HTML
                $('#tasks').append(

                //CREATE ITEM[i] HEADER
                $('<tr>')
                .addClass('panel panel-default clickable hr') //hr class is super important in css
                .attr("data-target", id)
                .attr('data-toggle', 'collapse')
                .attr("id", "accordion" + id)
                .append(
                    //TASK TITLE
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<h4>')
                            .append(
                                    $('<a>')
                                    .attr("data-toggle", "collapse")
                                    .attr("data-parent", "#accordion" + id)
                                    .attr('href', '#' + id)
                                    .append(name))))
                .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<span>')
                            .addClass('stars')
                            .html(totalRatings)))
                .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<h4>')
                            .html((deadline.getMonth() + 1) + "/" + deadline.getDate() + "/" + deadline.getFullYear())))
                .append(
                    //SHOW ASSIGNED USER
                    $('<td>')
                            .addClass('panel panel-heading')
                            .append(
                                $('<h4>')
                                    .html(assignedUser)))

                .append(
                    //CREATE BUTTONS 
                    $('<td>')
                            .addClass('panel panel-heading block')
                            .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-danger delete-task" data-toggle="tooltip" title="Delete Task"><span class="glyphicon glyphicon-trash"></span></a>'))
                            .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-info edit-task"><span class="glyphicon glyphicon-pencil" data-toggle="tooltip" title="Edit Task"></span></a>'))
                            .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm assign-user"><span class="glyphicon glyphicon-user" data-toggle="tooltip" title="Assign User"></span></a>'))
                            .append($('<a href="#" data-tid="' + id + rateButton))
                    ));

                //CREATE ITEM[i] COLLAPSIBLE CONTENT
                $('#tasks').append(
                    $('<tr>')
                    .attr("id", id)
                    .addClass('panel-collapse collapse')
                    .append(
                    $('<td>')
                        .attr("colspan", 4)
                        .addClass('panel panel-body')
                        .append(
                            $('<p>')
                            .addClass('item-header')
                            .append("Description:"))
                            .append(
                            $('<p>')
                            .append(description))));


                //Check if Task is Done
                if (results[i].get("done")) {
                    if (i == 0) {
                        $('#tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                    else {
                        $('#tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                }

                else {
                    //Check if Task is Overdue

                    if (deadline < date) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                        else {
                            $('#tasks tr:eq(' + (i * 2) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                    }

                    //Check if Task is due soon (within 2 days)
                    var date2 = new Date(date).setDate(date.getDate() + 2);
                    if (deadline > date && deadline < date2) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                        else {
                            $('#tasks tr:eq(' + (i * 2) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                    }
                }
                //Check if Task is Done
                if (results[i].get("done")) {
                    if (i == 0) {
                        $('#tasks tr:eq(' + (i + 1) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                    else {
                        $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                }

                else {
                    //Check if Task is Overdue

                    if (deadline < date) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                        else {
                            $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                    }

                    //Check if Task is due soon (within 2 days)
                    var date2 = new Date(date).setDate(date.getDate() + 2);
                    if (deadline > date && deadline < date2) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                        else {
                            $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                    }
                }
            }// END FOR LOOP
		}
	}).then(function (rows) {
	    $('.stars').stars();
	});
}

function sortByAssignedTo() {
	event.preventDefault();
	$("#section").html(_.template($('#project-task-view').html()));
	var query = new Parse.Query("Task");

	byAssignedTo = !byAssignedTo;

	switch (localStorage.selectVal) {
        case "all":
            var query = new Parse.Query("Task");
            break;
        case "inprogress":
            query.equalTo("done", false);
            break;
        case "completed":
            query.equalTo("done", true);
            break;
        case "overdue":
            query.equalTo("done", false);
            query.lessThan("taskDeadline", date);
            break;
        case "mine":
            query.equalTo("assignedTo", Parse.User.current());
            break;
    }

    if(byAssignedTo) {
		query.ascending("assignedTo");
	}
	else {
		query.descending("assignedTo");
	}

	if(Global_Project.id == null) {
    	query.equalTo("assignedTo", user);
    }
    else {
    	query.equalTo("projectPointer", Global_Project);
    }

    query.equalTo("projectPointer", Global_Project);
	query.find({
        //for collapsible task view
        success: function (results) {

            for (var i = 0; i < results.length; i++) {
                var task = results[i];
                var name = task.get("taskName");
                var id = task.id;
                var description = task.get("taskDescription");
                var deadline = task.get("taskDeadline");
                var assigned = new Parse.User();
                assigned = task.get("assignedTo");
                ratings = task.get("totalRatings");
                var totalRatings = 0;

                if (ratings == null) {
                    totalRatings = 0;
                }
                else {
                    for (var j = 0; j < ratings.length; j++) {
                        totalRatings = (totalRatings + ratings[j].rating);
                    }
                    totalRatings = totalRatings / ratings.length;
                    totalRatings = totalRatings.toFixed(2);
                }

                if (task.get("assignedTo") != null) {
                    var assignedUser = assigned.get("username");
                }
                else {
                    var assignedUser = "Unassigned";
                }

                //To change between the rate task button and mark complete button
                if(assignedUser != Parse.User.current().get("username")) {
                    rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-stats" data-toggle="tooltip" title="Rating Tasks"></span></a>';
                }
                else {
                    rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="Mark Completed"></span></a>';
                }

                //Select the blank table built into the HTML
                $('#tasks').append(

                //CREATE ITEM[i] HEADER
                $('<tr>')
                .addClass('panel panel-default clickable hr') //hr class is super important in css
                .attr("data-target", id)
                .attr('data-toggle', 'collapse')
                .attr("id", "accordion" + id)
                .append(
                    //TASK TITLE
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<h4>')
                            .append(
                                    $('<a>')
                                    .attr("data-toggle", "collapse")
                                    .attr("data-parent", "#accordion" + id)
                                    .attr('href', '#' + id)
                                    .append(name))))
                .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<span>')
                            .addClass('stars')
                            .html(totalRatings)))
                .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<h4>')
                            .html((deadline.getMonth() + 1) + "/" + deadline.getDate() + "/" + deadline.getFullYear())))
                .append(
                    //SHOW ASSIGNED USER
                    $('<td>')
                            .addClass('panel panel-heading')
                            .append(
                                $('<h4>')
                                    .html(assignedUser)))

                .append(
                    //CREATE BUTTONS 
                    $('<td>')
                            .addClass('panel panel-heading block')
                            .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-danger delete-task" data-toggle="tooltip" title="Delete Task"><span class="glyphicon glyphicon-trash"></span></a>'))
                            .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-info edit-task"><span class="glyphicon glyphicon-pencil" data-toggle="tooltip" title="Edit Task"></span></a>'))
                            .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm assign-user"><span class="glyphicon glyphicon-user" data-toggle="tooltip" title="Assign User"></span></a>'))
                            .append($('<a href="#" data-tid="' + id + rateButton))
                    ));

                //CREATE ITEM[i] COLLAPSIBLE CONTENT
                $('#tasks').append(
                    $('<tr>')
                    .attr("id", id)
                    .addClass('panel-collapse collapse')
                    .append(
                    $('<td>')
                        .attr("colspan", 4)
                        .addClass('panel panel-body')
                        .append(
                            $('<p>')
                            .addClass('item-header')
                            .append("Description:"))
                            .append(
                            $('<p>')
                            .append(description))));


                //Check if Task is Done
                if (results[i].get("done")) {
                    if (i == 0) {
                        $('#tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                    else {
                        $('#tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                }

                else {
                    //Check if Task is Overdue

                    if (deadline < date) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                        else {
                            $('#tasks tr:eq(' + (i * 2) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                    }

                    //Check if Task is due soon (within 2 days)
                    var date2 = new Date(date).setDate(date.getDate() + 2);
                    if (deadline > date && deadline < date2) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                        else {
                            $('#tasks tr:eq(' + (i * 2) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                    }
                }
                //Check if Task is Done
                if (results[i].get("done")) {
                    if (i == 0) {
                        $('#tasks tr:eq(' + (i + 1) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                    else {
                        $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                }

                else {
                    //Check if Task is Overdue

                    if (deadline < date) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                        else {
                            $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                    }

                    //Check if Task is due soon (within 2 days)
                    var date2 = new Date(date).setDate(date.getDate() + 2);
                    if (deadline > date && deadline < date2) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                        else {
                            $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                    }
                }
            }// END FOR LOOP
		}
	}).then(function (rows) {
	    $('.stars').stars();
	});
}

function mySortByName() {
	event.preventDefault();
	$("#section").html(_.template($('#usertask-view').html()));
	var query = new Parse.Query("Task");

	byName = !byName;

	$('#task-filter').val(localStorage.selectVal);

    switch (localStorage.selectVal) {
        case "all":
            var query = new Parse.Query("Task");
            break;
        case "inprogress":
            query.equalTo("done", false);
            break;
        case "completed":
            query.equalTo("done", true);
            break;
        case "overdue":
            query.equalTo("done", false);
            query.lessThan("taskDeadline", date);
            break;
        case "mine":
            query.equalTo("assignedTo", Parse.User.current());
            break;
    }

    if(byName) {
		query.ascending("taskName");
	}
	else {
		query.descending("taskName");
	}

    query.equalTo("assignedTo", user);

	query.find({
        //for collapsible task view
        success: function (results) {

            for (var i = 0; i < results.length; i++) {
                var task = results[i];
                var name = task.get("taskName");
                var id = task.id;
                var description = task.get("taskDescription");
                var deadline = task.get("taskDeadline");
                var project = task.get("projectPointer")
                var assigned = new Parse.User();
                assigned = task.get("assignedTo");
                ratings = task.get("totalRatings");
                var totalRatings = 0;

                if (ratings == null) {
                    totalRatings = 0;
                }
                else {
                    for (var j = 0; j < ratings.length; j++) {
                        totalRatings = (totalRatings + ratings[j].rating);
                    }
                    totalRatings = totalRatings / ratings.length;
                    totalRatings = totalRatings.toFixed(2);
                }

                if (task.get("assignedTo") != null) {
                    var assignedUser = assigned.get("username");
                }
                else {
                    var assignedUser = "Unassigned";
                }

                //To change between the rate task button and mark complete button
                if(assignedUser != Parse.User.current().get("username")) {
                    rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-stats" data-toggle="tooltip" title="Rating Tasks"></span></a>';
                }
                else {
                    rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="Mark Completed"></span></a>';
                }

                //Select the blank table built into the HTML
                $('#user-tasks').append(

                    //CREATE ITEM[i] HEADER
                    $('<tr>')
                    .addClass('panel panel-default clickable hr') //hr class is super important in css
                    .attr("data-target", id)
                    .attr('data-toggle', 'collapse')
                    .attr("id", "accordion" + id)
                    .append(

                        //TASK TITLE
                        $('<td>')
                            .addClass('panel panel-heading')
                            .append(
                                $('<h4>')
                                .append(
                                        $('<a>')
                                        .attr("data-toggle", "collapse")
                                        .attr("data-parent", "#accordion" + id)
                                        .attr('href', '#' + id)
                                        .append(name))))
                    .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<span>')
                            .addClass('stars')
                            .html(totalRatings)))
                    .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<h4>')
                            .html((deadline.getMonth() + 1) + "/" + deadline.getDate() + "/" + deadline.getFullYear())))
                    .append(
                    	$('<td>')
                    		.addClass('panel panel-heading')
                    		.append(
                    			$('<h4>')
                    			.html(project.get("projectName"))))
                    .append(
                        //CREATE BUTTONS 
                        $('<td>')
                                .addClass('panel panel-heading block')
                                .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-danger delete-task" data-toggle="tooltip" title="Delete Task"><span class="glyphicon glyphicon-trash"></span></a>'))
                                .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-info edit-task"><span class="glyphicon glyphicon-pencil" data-toggle="tooltip" title="Edit Task"></span></a>'))
                                .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="Mark Completed"></span></a>'))
                        ));

                //CREATE ITEM[i] COLLAPSIBLE CONTENT
                $('#user-tasks').append(
                    $('<tr>')
                    .attr("id", id)
                    .addClass('panel-collapse collapse')
                    .append(
                    $('<td>')
                        .attr("colspan", 4)
                        .addClass('panel panel-body')
                        .append(
                            $('<p>')
                            .addClass('item-header')
                            .append("Description:"))
                            .append(
                            $('<p>')
                            .append(description))
                            .append(
                            $('<p>')
                            .addClass('item-header'))));


                //Check if Task is Done
                if (results[i].get("done")) {
                    if (i == 0) {
                        $('#user-tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                    else {
                        $('#user-tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                }

                //Check if Task is Overdue

                if (deadline < date) {
                    if (i == 0) {
                        $('#user-tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-danger');
                    }
                    else {
                        $('#user-tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-danger');
                    }
                }

                //Check if Task is due soon (within 2 days)
                var date2 = new Date(date).setDate(date.getDate() + 2);
                if (deadline > date && deadline < date2) {
                    if (i == 0) {
                        $('#user-tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-warning');
                        console.log("SHITS CLOSE DAWG");
                    }
                    else {
                        $('#user-tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-warning');
                    }
                }
                //Check if Task is Done
                if (results[i].get("done")) {
                    if (i == 0) {
                        $('#tasks tr:eq(' + (i + 1) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                    else {
                        $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                }

                else {
                    //Check if Task is Overdue

                    if (deadline < date) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                        else {
                            $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                    }

                    //Check if Task is due soon (within 2 days)
                    var date2 = new Date(date).setDate(date.getDate() + 2);
                    if (deadline > date && deadline < date2) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                        else {
                            $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                    }
                }
            }// END FOR LOOP
		}
	}).then(function (rows) {
	    $('.stars').stars();
	});
}
function mySortByRating() {
	event.preventDefault();
	$("#section").html(_.template($('#usertask-view').html()));
	var query = new Parse.Query("Task");

	byRating = !byRating;

	$('#task-filter').val(localStorage.selectVal);

    switch (localStorage.selectVal) {
        case "all":
            var query = new Parse.Query("Task");
            break;
        case "inprogress":
            query.equalTo("done", false);
            break;
        case "completed":
            query.equalTo("done", true);
            break;
        case "overdue":
            query.equalTo("done", false);
            query.lessThan("taskDeadline", date);
            break;
        case "mine":
            query.equalTo("assignedTo", Parse.User.current());
            break;
    }

    if(byRating) {
		query.ascending("totalRatings");
	}
	else {
		query.descending("totalRatings");
	}

    query.equalTo("assignedTo", user);

	query.find({
        //for collapsible task view
        success: function (results) {

            for (var i = 0; i < results.length; i++) {
                var task = results[i];
                var name = task.get("taskName");
                var id = task.id;
                var description = task.get("taskDescription");
                var deadline = task.get("taskDeadline");
                var project = task.get("projectPointer")
                var assigned = new Parse.User();
                assigned = task.get("assignedTo");
                ratings = task.get("totalRatings");
                var totalRatings = 0;

                if (ratings == null) {
                    totalRatings = 0;
                }
                else {
                    for (var j = 0; j < ratings.length; j++) {
                        totalRatings = (totalRatings + ratings[j].rating);
                    }
                    totalRatings = totalRatings / ratings.length;
                    totalRatings = totalRatings.toFixed(2);
                }

                if (task.get("assignedTo") != null) {
                    var assignedUser = assigned.get("username");
                }
                else {
                    var assignedUser = "Unassigned";
                }

                //To change between the rate task button and mark complete button
                if(assignedUser != Parse.User.current().get("username")) {
                    rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-stats" data-toggle="tooltip" title="Rating Tasks"></span></a>';
                }
                else {
                    rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="Mark Completed"></span></a>';
                }

                //Select the blank table built into the HTML
                $('#user-tasks').append(

                    //CREATE ITEM[i] HEADER
                    $('<tr>')
                    .addClass('panel panel-default clickable hr') //hr class is super important in css
                    .attr("data-target", id)
                    .attr('data-toggle', 'collapse')
                    .attr("id", "accordion" + id)
                    .append(

                        //TASK TITLE
                        $('<td>')
                            .addClass('panel panel-heading')
                            .append(
                                $('<h4>')
                                .append(
                                        $('<a>')
                                        .attr("data-toggle", "collapse")
                                        .attr("data-parent", "#accordion" + id)
                                        .attr('href', '#' + id)
                                        .append(name))))
                    .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<span>')
                            .addClass('stars')
                            .html(totalRatings)))
                    .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<h4>')
                            .html((deadline.getMonth() + 1) + "/" + deadline.getDate() + "/" + deadline.getFullYear())))
                    .append(
                    	$('<td>')
                    		.addClass('panel panel-heading')
                    		.append(
                    			$('<h4>')
                    			.html(project.get("projectName"))))
                    .append(
                        //CREATE BUTTONS 
                        $('<td>')
                                .addClass('panel panel-heading block')
                                .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-danger delete-task" data-toggle="tooltip" title="Delete Task"><span class="glyphicon glyphicon-trash"></span></a>'))
                                .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-info edit-task"><span class="glyphicon glyphicon-pencil" data-toggle="tooltip" title="Edit Task"></span></a>'))
                                .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="Mark Completed"></span></a>'))
                        ));

                //CREATE ITEM[i] COLLAPSIBLE CONTENT
                $('#user-tasks').append(
                    $('<tr>')
                    .attr("id", id)
                    .addClass('panel-collapse collapse')
                    .append(
                    $('<td>')
                        .attr("colspan", 4)
                        .addClass('panel panel-body')
                        .append(
                            $('<p>')
                            .addClass('item-header')
                            .append("Description:"))
                            .append(
                            $('<p>')
                            .append(description))
                            .append(
                            $('<p>')
                            .addClass('item-header'))));


                //Check if Task is Done
                if (results[i].get("done")) {
                    if (i == 0) {
                        $('#user-tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                    else {
                        $('#user-tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                }

                //Check if Task is Overdue

                if (deadline < date) {
                    if (i == 0) {
                        $('#user-tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-danger');
                    }
                    else {
                        $('#user-tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-danger');
                    }
                }

                //Check if Task is due soon (within 2 days)
                var date2 = new Date(date).setDate(date.getDate() + 2);
                if (deadline > date && deadline < date2) {
                    if (i == 0) {
                        $('#user-tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-warning');
                        console.log("SHITS CLOSE DAWG");
                    }
                    else {
                        $('#user-tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-warning');
                    }
                }
                //Check if Task is Done
                if (results[i].get("done")) {
                    if (i == 0) {
                        $('#tasks tr:eq(' + (i + 1) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                    else {
                        $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                }

                else {
                    //Check if Task is Overdue

                    if (deadline < date) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                        else {
                            $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                    }

                    //Check if Task is due soon (within 2 days)
                    var date2 = new Date(date).setDate(date.getDate() + 2);
                    if (deadline > date && deadline < date2) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                        else {
                            $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                    }
                }
            }// END FOR LOOP
		}
	}).then(function (rows) {
	    $('.stars').stars();
	});
}
function mySortByDeadline() {
	event.preventDefault();
	$("#section").html(_.template($('#usertask-view').html()));
	var query = new Parse.Query("Task");

	byDeadline = !byDeadline;

	$('#task-filter').val(localStorage.selectVal);

    switch (localStorage.selectVal) {
        case "all":
            var query = new Parse.Query("Task");
            break;
        case "inprogress":
            query.equalTo("done", false);
            break;
        case "completed":
            query.equalTo("done", true);
            break;
        case "overdue":
            query.equalTo("done", false);
            query.lessThan("taskDeadline", date);
            break;
        case "mine":
            query.equalTo("assignedTo", Parse.User.current());
            break;
    }

    if(byDeadline) {
		query.ascending("taskDeadline");
	}
	else {
		query.descending("taskDeadline");
	}

    query.equalTo("assignedTo", user);

	query.find({
        //for collapsible task view
        success: function (results) {

            for (var i = 0; i < results.length; i++) {
                var task = results[i];
                var name = task.get("taskName");
                var id = task.id;
                var description = task.get("taskDescription");
                var deadline = task.get("taskDeadline");
                var project = task.get("projectPointer")
                var assigned = new Parse.User();
                assigned = task.get("assignedTo");
                ratings = task.get("totalRatings");
                var totalRatings = 0;

                if (ratings == null) {
                    totalRatings = 0;
                }
                else {
                    for (var j = 0; j < ratings.length; j++) {
                        totalRatings = (totalRatings + ratings[j].rating);
                    }
                    totalRatings = totalRatings / ratings.length;
                    totalRatings = totalRatings.toFixed(2);
                }

                if (task.get("assignedTo") != null) {
                    var assignedUser = assigned.get("username");
                }
                else {
                    var assignedUser = "Unassigned";
                }

                //To change between the rate task button and mark complete button
                if(assignedUser != Parse.User.current().get("username")) {
                    rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-stats" data-toggle="tooltip" title="Rating Tasks"></span></a>';
                }
                else {
                    rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="Mark Completed"></span></a>';
                }

                //Select the blank table built into the HTML
                $('#user-tasks').append(

                    //CREATE ITEM[i] HEADER
                    $('<tr>')
                    .addClass('panel panel-default clickable hr') //hr class is super important in css
                    .attr("data-target", id)
                    .attr('data-toggle', 'collapse')
                    .attr("id", "accordion" + id)
                    .append(

                        //TASK TITLE
                        $('<td>')
                            .addClass('panel panel-heading')
                            .append(
                                $('<h4>')
                                .append(
                                        $('<a>')
                                        .attr("data-toggle", "collapse")
                                        .attr("data-parent", "#accordion" + id)
                                        .attr('href', '#' + id)
                                        .append(name))))
                    .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<span>')
                            .addClass('stars')
                            .html(totalRatings)))
                    .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<h4>')
                            .html((deadline.getMonth() + 1) + "/" + deadline.getDate() + "/" + deadline.getFullYear())))
                    .append(
                    	$('<td>')
                    		.addClass('panel panel-heading')
                    		.append(
                    			$('<h4>')
                    			.html(project.get("projectName"))))
                    .append(
                        //CREATE BUTTONS 
                        $('<td>')
                                .addClass('panel panel-heading block')
                                .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-danger delete-task" data-toggle="tooltip" title="Delete Task"><span class="glyphicon glyphicon-trash"></span></a>'))
                                .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-info edit-task"><span class="glyphicon glyphicon-pencil" data-toggle="tooltip" title="Edit Task"></span></a>'))
                                .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="Mark Completed"></span></a>'))
                        ));

                //CREATE ITEM[i] COLLAPSIBLE CONTENT
                $('#user-tasks').append(
                    $('<tr>')
                    .attr("id", id)
                    .addClass('panel-collapse collapse')
                    .append(
                    $('<td>')
                        .attr("colspan", 4)
                        .addClass('panel panel-body')
                        .append(
                            $('<p>')
                            .addClass('item-header')
                            .append("Description:"))
                            .append(
                            $('<p>')
                            .append(description))
                            .append(
                            $('<p>')
                            .addClass('item-header'))));


                //Check if Task is Done
                if (results[i].get("done")) {
                    if (i == 0) {
                        $('#user-tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                    else {
                        $('#user-tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                }

                //Check if Task is Overdue

                if (deadline < date) {
                    if (i == 0) {
                        $('#user-tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-danger');
                    }
                    else {
                        $('#user-tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-danger');
                    }
                }

                //Check if Task is due soon (within 2 days)
                var date2 = new Date(date).setDate(date.getDate() + 2);
                if (deadline > date && deadline < date2) {
                    if (i == 0) {
                        $('#user-tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-warning');
                        console.log("SHITS CLOSE DAWG");
                    }
                    else {
                        $('#user-tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-warning');
                    }
                }
                //Check if Task is Done
                if (results[i].get("done")) {
                    if (i == 0) {
                        $('#tasks tr:eq(' + (i + 1) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                    else {
                        $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                }

                else {
                    //Check if Task is Overdue

                    if (deadline < date) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                        else {
                            $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                    }

                    //Check if Task is due soon (within 2 days)
                    var date2 = new Date(date).setDate(date.getDate() + 2);
                    if (deadline > date && deadline < date2) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                        else {
                            $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                    }
                }
            }// END FOR LOOP
		}
	}).then(function (rows) {
	    $('.stars').stars();
	});
}

function mySortByProjectName() {
	event.preventDefault();
	$("#section").html(_.template($('#usertask-view').html()));
	var query = new Parse.Query("Task");

	byAssignedTo = !byAssignedTo;

	$('#task-filter').val(localStorage.selectVal);

    switch (localStorage.selectVal) {
        case "all":
            var query = new Parse.Query("Task");
            break;
        case "inprogress":
            query.equalTo("done", false);
            break;
        case "completed":
            query.equalTo("done", true);
            break;
        case "overdue":
            query.equalTo("done", false);
            query.lessThan("taskDeadline", date);
            break;
        case "mine":
            query.equalTo("assignedTo", Parse.User.current());
            break;
    }

    if(byAssignedTo) {
		query.ascending("projectPointer");
	}
	else {
		query.descending("projectPointer");
	}

    query.equalTo("assignedTo", user);

	query.find({
        //for collapsible task view
        success: function (results) {

            for (var i = 0; i < results.length; i++) {
                var task = results[i];
                var name = task.get("taskName");
                var id = task.id;
                var description = task.get("taskDescription");
                var deadline = task.get("taskDeadline");
                var project = task.get("projectPointer")
                var assigned = new Parse.User();
                assigned = task.get("assignedTo");
                ratings = task.get("totalRatings");
                var totalRatings = 0;

                if (ratings == null) {
                    totalRatings = 0;
                }
                else {
                    for (var j = 0; j < ratings.length; j++) {
                        totalRatings = (totalRatings + ratings[j].rating);
                    }
                    totalRatings = totalRatings / ratings.length;
                    totalRatings = totalRatings.toFixed(2);
                }

                if (task.get("assignedTo") != null) {
                    var assignedUser = assigned.get("username");
                }
                else {
                    var assignedUser = "Unassigned";
                }

                //To change between the rate task button and mark complete button
                if(assignedUser != Parse.User.current().get("username")) {
                    rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-stats" data-toggle="tooltip" title="Rating Tasks"></span></a>';
                }
                else {
                    rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="Mark Completed"></span></a>';
                }

                //Select the blank table built into the HTML
                $('#user-tasks').append(

                    //CREATE ITEM[i] HEADER
                    $('<tr>')
                    .addClass('panel panel-default clickable hr') //hr class is super important in css
                    .attr("data-target", id)
                    .attr('data-toggle', 'collapse')
                    .attr("id", "accordion" + id)
                    .append(

                        //TASK TITLE
                        $('<td>')
                            .addClass('panel panel-heading')
                            .append(
                                $('<h4>')
                                .append(
                                        $('<a>')
                                        .attr("data-toggle", "collapse")
                                        .attr("data-parent", "#accordion" + id)
                                        .attr('href', '#' + id)
                                        .append(name))))
                    .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<span>')
                            .addClass('stars')
                            .html(totalRatings)))
                    .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<h4>')
                            .html((deadline.getMonth() + 1) + "/" + deadline.getDate() + "/" + deadline.getFullYear())))
                    .append(
                    	$('<td>')
                    		.addClass('panel panel-heading')
                    		.append(
                    			$('<h4>')
                    			.html(project.get("projectName"))))
                    .append(
                        //CREATE BUTTONS 
                        $('<td>')
                                .addClass('panel panel-heading block')
                                .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-danger delete-task" data-toggle="tooltip" title="Delete Task"><span class="glyphicon glyphicon-trash"></span></a>'))
                                .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-info edit-task"><span class="glyphicon glyphicon-pencil" data-toggle="tooltip" title="Edit Task"></span></a>'))
                                .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="Mark Completed"></span></a>'))
                        ));

                //CREATE ITEM[i] COLLAPSIBLE CONTENT
                $('#user-tasks').append(
                    $('<tr>')
                    .attr("id", id)
                    .addClass('panel-collapse collapse')
                    .append(
                    $('<td>')
                        .attr("colspan", 4)
                        .addClass('panel panel-body')
                        .append(
                            $('<p>')
                            .addClass('item-header')
                            .append("Description:"))
                            .append(
                            $('<p>')
                            .append(description))
                            .append(
                            $('<p>')
                            .addClass('item-header'))));


                //Check if Task is Done
                if (results[i].get("done")) {
                    if (i == 0) {
                        $('#user-tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                    else {
                        $('#user-tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                }

                //Check if Task is Overdue

                if (deadline < date) {
                    if (i == 0) {
                        $('#user-tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-danger');
                    }
                    else {
                        $('#user-tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-danger');
                    }
                }

                //Check if Task is due soon (within 2 days)
                var date2 = new Date(date).setDate(date.getDate() + 2);
                if (deadline > date && deadline < date2) {
                    if (i == 0) {
                        $('#user-tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-warning');
                        console.log("SHITS CLOSE DAWG");
                    }
                    else {
                        $('#user-tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-warning');
                    }
                }
                //Check if Task is Done
                if (results[i].get("done")) {
                    if (i == 0) {
                        $('#tasks tr:eq(' + (i + 1) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                    else {
                        $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                }

                else {
                    //Check if Task is Overdue

                    if (deadline < date) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                        else {
                            $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                    }

                    //Check if Task is due soon (within 2 days)
                    var date2 = new Date(date).setDate(date.getDate() + 2);
                    if (deadline > date && deadline < date2) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                        else {
                            $('#tasks tr:eq(' + ((i * 2) + 1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                    }
                }
            }// END FOR LOOP
		}
	}).then(function (rows) {
	    $('.stars').stars();
	});
}

