function loadProjectTasks() {
    event.preventDefault();
    $("#section").html(_.template($('#project-task-view').html()));
    $(".datepicker").datepicker();
    date = new Date();
    var user = Parse.User.current();
    var query = new Parse.Query("Task");
    var ratings = [];

    $('#task-filter').val(localStorage.selectVal);

    switch (localStorage.selectVal) {
        case "all":
            var query = new Parse.Query("Task");
            break;
        case "inprogress":
            query.equalTo("done", false);
            break;
        case "completed":
            query.equalTo("done", true);
            break;
        case "overdue":
            query.equalTo("done", false);
            query.lessThan("taskDeadline", date);
            break;
        case "mine":
            query.equalTo("assignedTo", Parse.User.current());
            break;
    }

    query.equalTo("projectPointer", Global_Project);
    query.find({
        //for collapsible task view
        success: function (results) {

            for (var i = 0; i < results.length; i++) {
                var task = results[i];
                var name = task.get("taskName");
                var id = task.id;
                var description = task.get("taskDescription");
                var deadline = task.get("taskDeadline");
                var assigned = new Parse.User();
                assigned = task.get("assignedTo");
                ratings = task.get("totalRatings");
                var totalRatings = 0;

                if (ratings == null) {
                    totalRatings = 0;
                }
                else {
                    for (var j = 0; j < ratings.length; j++) {
                        totalRatings = (totalRatings + ratings[j].rating);
                    }
                    totalRatings = totalRatings / ratings.length;
                    totalRatings = totalRatings.toFixed(2);
                }

                if (task.get("assignedTo") != null) {
                    var assignedUser = assigned.get("username");
                }
                else {
                    var assignedUser = "Unassigned";
                }

                //To change between the rate task button and mark complete button
                if(assignedUser != Parse.User.current().get("username")) {
                    rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-stats" data-toggle="tooltip" title="Rating Tasks"></span></a>';
                }
                else {
                    rateButton = '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="Mark Completed"></span></a>';
                }

                //Select the blank table built into the HTML
                $('#tasks').append(

                //CREATE ITEM[i] HEADER
                $('<tr>')
                .addClass('panel panel-default clickable hr') //hr class is super important in css
                .attr("data-target", id)
                .attr('data-toggle', 'collapse')
                .attr("id", "accordion" + id)
                .append(
                    //TASK TITLE
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<h4>')
                            .append(
                                    $('<a>')
                                    .attr("data-toggle", "collapse")
                                    .attr("data-parent", "#accordion" + id)
                                    .attr('href', '#' + id)
                                    .append(name))))
                    //TASK RATING
                .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<span>')
                            .addClass('stars')
                            .html(totalRatings)))
                .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<h4>')
                            .html((deadline.getMonth() + 1) + "/" + deadline.getDate() + "/" + deadline.getFullYear())))
                .append(
                    //SHOW ASSIGNED USER
                    $('<td>')
                            .addClass('panel panel-heading')
                            .append(
                                $('<h4>')
                                    .html(assignedUser)))

                .append(
                    //CREATE BUTTONS 
                    $('<td>')
                            .addClass('panel panel-heading block')
                            .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-danger delete-task" data-toggle="tooltip" title="Delete Task"><span class="glyphicon glyphicon-trash"></span></a>'))
                            .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-info edit-task"><span class="glyphicon glyphicon-pencil" data-toggle="tooltip" title="Edit Task"></span></a>'))
                            .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm assign-user"><span class="glyphicon glyphicon-user" data-toggle="tooltip" title="Assign User"></span></a>'))
                            .append($('<a href="#" data-tid="' + id + rateButton))
                    ));

                //CREATE ITEM[i] COLLAPSIBLE CONTENT
                $('#tasks').append(
                    $('<tr>')
                    .attr("id", id)
                    .addClass('panel-collapse collapse')
                    .append(
                    $('<td>')
                        .attr("colspan", 4)
                        .addClass('panel panel-body')
                        .append(
                            $('<p>')
                            .addClass('item-header')
                            .append("Description:"))
                            .append(
                            $('<p>')
                            .append(description))));


                //Check if Task is Done
                if (results[i].get("done")) {
                    if (i == 0) {
                        $('#tasks tr:eq(' + (i+1) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                    else {
                        $('#tasks tr:eq(' + ((i * 2)+1) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                }

                else {
                    //Check if Task is Overdue

                    if (deadline < date) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i+1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                        else {
                            $('#tasks tr:eq(' + ((i * 2)+1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-danger');
                        }
                    }

                    //Check if Task is due soon (within 2 days)
                    var date2 = new Date(date).setDate(date.getDate() + 2);
                    if (deadline > date && deadline < date2) {
                        if (i == 0) {
                            $('#tasks tr:eq(' + (i+1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                        else {
                            $('#tasks tr:eq(' + ((i * 2)+1) + ')')
                                .removeClass("panel-heading")
                                .addClass('panel-warning');
                        }
                    }
                }
            }// END FOR LOOP
        }
    }).then(function (rows) {
        $('.stars').stars();
    });
}

function assignUserModal($tid) {
    event.preventDefault();
    id = $tid.data("tid");
    $('#assignee-list').empty();
    $('.assignUserBox form').empty();
    $('.assignUserBox form').append(
        $('<select>')
        .attr("id", "assignee-list"));
    Role = Global_Project.get("projectRole");
    var relation = Role.getUsers();
    var query = relation.query();

    $('#assignee-list').append(
       $("<option>", {
           text: ("Unassigned"),
           value: (null)
       }));

    query.find({
        success: function (results) {
            // Do something with the returned Parse.Object values
            for (var i = 0; i < results.length; i++) {
                var object = results[i].id;
                query = new Parse.Query("_User");
                query.equalTo("objectId", object);
                query.find({

                    success: function (results) {
                        for (var i = 0; i < results.length; i++) {
                            $('#assignee-list').append(
                                $("<option>", {
                                    text: (results[i].get("username")),
                                    value: (results[i].id)
                                }));
                        }
                    },
                    error: function (error) {
                        cosole.log(error);
                    }
                });
            }
        },
        error: function (error) {
            alert("Error: " + error.code + " " + error.message);
        }
    });

    $('.assignUserBox form').append(
        $('<button>')
            .attr('type', "button")
            .addClass('btn btn-primary btn-sm')
            .addClass('assignUserBtn')
            .html('Submit'));

    $('#assign-user').modal('toggle');

    //TODO - ASSIGN USER!
}

function assignUser(id) {
    if (id == null) {
        Current_Task.unset("assignedTo");
        Current_Task.save(null, {
            success: function (task) {
                //Task was saved
                $('#assign-user').modal('toggle');
                var message = new Parse.Object("Message");
                message.set("message", Current_Task.get("taskName") + " has been unassigned.");
                message.set("postBy", Parse.User.current());
                message.set("projectPointer", Global_Project);
                message.setACL(Global_Project.getACL());
                message.save(null, {
                    success: function (message) {
                        loadProjectTasks();
                    },
                    error: function (message, error) {
                        console.log("ERROR!  " + error);
                    }
                });
            },
            error: function (task, error) {
                //Failed to Save Project
                console.log("Error!" + task + error.code + " " + error.message);
            }
        });
    }
    else {
        uid = id;
        user = new Parse.User();
        user.id = uid;
        var previous_User = Current_Task.get("assignedTo");
        if (previous_User == null) {
            Current_Task.fetch({
                success: function (task) {
                    Current_Task.set("assignedTo", user);
                    Current_Task.save(null, {
                        success: function (task) {
                            //Task was saved
                            $('#assign-user').modal('toggle');
                            var message = new Parse.Object("Message");
                            message.set("message", Current_Task.get("taskName") + " has been assigned to " + user.get("username") + ".");
                            message.set("postBy", Parse.User.current());
                            message.set("projectPointer", Global_Project);
                            message.setACL(Global_Project.getACL());
                            message.save(null, {
                                success: function (message) {
                                    loadProjectTasks();
                                },
                                error: function (message, error) {
                                    console.log("ERROR!  " + error);
                                }
                            });
                        },
                        error: function (task, error) {
                            //Failed to Save Project
                            console.log("Error!" + task + error.code + " " + error.message);
                        }
                    });
                }
            });
        }
        else {
            Current_Task.fetch({
                success: function (task) {
                    Current_Task.set("assignedTo", user);

                    Current_Task.save(null, {
                        success: function (task) {
                            //Task was saved
                            $('#assign-user').modal('toggle');
                            var message = new Parse.Object("Message");
                            message.set("message", Current_Task.get("taskName") + " has been reassigned to " + user.get("username") + " from " + previous_User.get("username") + ".");
                            message.set("postBy", Parse.User.current());
                            message.set("projectPointer", Global_Project);
                            message.setACL(Global_Project.getACL());
                            message.save(null, {
                                success: function (message) {
                                    loadProjectTasks();
                                },
                                error: function (message, error) {
                                    console.log("ERROR!  " + error);
                                }
                            });
                        },
                        error: function (task, error) {
                            //Failed to Save Project
                            console.log("Error!" + task + error.code + " " + error.message);
                        }
                    });
                }
            });
        }
    }
}

function createTask() {

    //create a Task object
    var task = new Parse.Object("Task");
    taskName = $("#task-name").val();

    //Set Task Values with Form Data
    task.setACL(Global_Project.getACL());
    task.set("taskName", $("#task-name").val());
    task.set("taskDescription", $("#task-description").val());
    task.set("taskDeadline", $("#task-deadline").datepicker("getDate"));
    task.set("done", false);
    task.set("projectPointer", Global_Project);
    task.set("createdBy", Parse.User.current());
    task.save(null, {
        success: function (task) {
            $("#create-task form").trigger("reset");
            $("#create-task").modal("toggle");
            var message = new Parse.Object("Message");
            message.set("message", taskName + " was created.");
            message.set("postBy", Parse.User.current());
            message.set("projectPointer", Global_Project);
            message.setACL(Global_Project.getACL());
            message.save(null, {
                success: function (message) {
                    loadProjectTasks();
                },
                error: function (message, error) {
                    console.log("ERROR!  " + error);
                }
            });
        },
        error: function (task, error) {
            console.log("OH GOD TASK FAILED TO SAVE" + error);
        }
    });
}

function editTaskModal() {
    $('#edit-task').modal('toggle');
    $('#etask-name').attr("value", Current_Task.get("taskName"));
    $('#etask-description').attr("value", Current_Task.get("taskDescription"));
    $('#etask-name').html(Current_Task.get("taskName"));
    $('#etask-description').html(Current_Task.get("taskDescription"));
    var date = Current_Task.get("taskDeadline")
    $('#etask-deadline').datepicker();
    $('#etask-deadline').datepicker("setDate", date);

    //DISPLAY DATE NORMAL STYLE - (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear()
}

function editTask() {
    Current_Task.set("taskName", $('#etask-name').val());
    Current_Task.set("taskDescription", $('#etask-description').val());
    Current_Task.set("taskDeadline", $('#etask-deadline').datepicker("getDate"));
    Current_Task.save(null, {
        success: function (task) {
            $('#edit-task').modal('toggle');
            var message = new Parse.Object("Message");
            message.set("message", Current_Task.get("taskName") + " has been edited.");
            message.set("postBy", Parse.User.current());
            message.set("projectPointer", Global_Project);
            message.setACL(Global_Project.getACL());
            message.save(null, {
                success: function (message) {
                    loadProjectTasks();
                },
                error: function (message, error) {
                    console.log("ERROR!  " + error);
                }
            });
        },
        error: function (task, error) {
            console.log("OH GOD TASK FAILED TO SAVE", error);
        }
    });

}

function areYouSure() {
    $('#confirm-delete').modal('toggle');
}

function confirmComplete() {
    $('#confirm-complete .error').empty();
    $('#confirm-complete .error').removeClass('alert alert-danger')
    $('#confirm-complete').modal('toggle');
}



function deleteTask() {
    Current_Task.destroy({
        success: function (object) {
            $('#confirm-delete').modal('toggle');
            var message = new Parse.Object("Message");
            message.set("message", Current_Task.get("taskName") + " has been deleted.");
            message.set("postBy", Parse.User.current());
            message.set("projectPointer", Global_Project);
            message.setACL(Global_Project.getACL());
            message.save(null, {
                success: function (message) {
                    loadProjectTasks();
                },
                error: function (message, error) {
                    console.log("ERROR!  " + error);
                }
            });
        },
        error: function (error) {
            console.log("Error!  " + error.code + " : " + error.message);
        }
    })
}

function completeTask() {
    if (Current_Task.get("assignedTo") == null) {
        $('#confirm-complete .modal-dialog').addClass('shake');
        $('#confirm-complete .error').addClass('alert alert-danger').html("Task is not assigned!");
        setTimeout(function () {
            $('#confirm-complete .modal-dialog').removeClass('shake');
        }, 1000);
    }
    else if (Current_Task.get("assignedTo").id != Parse.User.current().id) {
        $('#confirm-complete .modal-dialog').addClass('shake');
        $('#confirm-complete .error').addClass('alert alert-danger').html("Task is not assigned to you!");
        setTimeout(function () {
            $('#confirm-complete .modal-dialog').removeClass('shake');
        }, 1000);
    }
    else {
        Current_Task.set("done", true);
        Current_Task.save({
            success: function (task) {
                $('#confirm-complete').modal('toggle');
                var message = new Parse.Object("Message");
                message.set("message", Current_Task.get("taskName") + " has been marked complete.");
                message.set("postBy", Parse.User.current());
                message.set("projectPointer", Current_Task.get("projectPointer"));
                message.setACL(CurrentTask.getACL());
                message.save(null, {
                    success: function (message) {
                        if(Global_Project.id == null) {
                            loadUserTasks();
                        }
                        else {
                            loadProjectTasks();
                        }
                    },
                    error: function (message, error) {
                        console.log("ERROR!  " + error);
                    }
                });
            },
            error: function (task, error) {
                console.log("Error!  " + error.code + " : " + error.message);
            }
        });
    }
}