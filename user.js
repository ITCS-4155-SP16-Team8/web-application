/* USER FUNCTIONS */

function loadUserHome() {
    event.preventDefault();
    Global_Project = new Parse.Object("Project");
    $("#project-list").children().removeClass("highlighted");
    $("#section").html(_.template($('#userhome-view').html()));
    $('#userheader').append(Parse.User.current().get("username"));
    $('#usericon').attr("src", Parse.User.current().get("picture").url());
    var query = new Parse.Query("Project");
    var myComm = 0;
    var myHelp = 0;
    var myPart = 0;
    var counter = 0;

    query.find({
        success: function (results) {
            for (var i = 0; i < results.length; i++) {
                var project = results[i];
                var review = [];
                review = project.get("peerReview");
                if (review == null) {
                    continue;
                }
                for (var j = 0; j < review.length; j++) {
                    if (review[j].ratee == Parse.User.current().id) {
                        myComm = myComm + review[j].communication;
                        myHelp = myHelp + review[j].helpfulness;
                        myPart = myPart + review[j].participation;
                        counter = counter + 1;
                    }
                }
            }
            if (counter == 0) {
                $('#communication-rating').html(0);
                $('#helpfulness-rating').html(0);
                $('#participation-rating').html(0);
                $('#overall-rating').html(0);
            }
            else {
                $('#communication-rating').html((myComm / counter).toFixed(2));
                $('#helpfulness-rating').html((myHelp / counter).toFixed(2));
                $('#participation-rating').html((myPart / counter).toFixed(2));
                $('#overall-rating').html((((myComm / counter) + (myHelp / counter) + (myPart / counter)) / 3).toFixed(2));
            }
            var query = new Parse.Query("Task");
            query.equalTo("assignedTo", user);

            query.find({
                success: function (tasks) {
                    var taskRatingArray = [];
                    var taskCounter = 0;
                    var totalRating = 0;
                    for (var i = 0; i < tasks.length; i++) {

                        taskRatingArray = tasks[i].get("totalRatings");

                        if (taskRatingArray != null) {
                            for (var j = 0; j < taskRatingArray.length; j++) {
                                totalRating = totalRating + taskRatingArray[j].rating;
                                taskCounter++;
                            }
                        }
                    }
                    if (taskCounter == 0) {
                        $('#user-task-rating').html(0);
                    }
                    else {
                        $('#user-task-rating').html((totalRating / taskCounter).toFixed(2));
                    }
                },
                error: function (results, error) {
                    console.log("Error pulling user tasks");
                }
            }).then(function (result) {
                $('.stars').stars();
            });
        },
        error: function (results, error) {
            console.log("Error!T%R@");
        }
    });
}


function loadUserTasks() {
    event.preventDefault();
    $("task-list").empty();
    $("#section").html(_.template($('#usertask-view').html()));
    $(".datepicker").datepicker();

    date = new Date();
    var user = Parse.User.current();
    var query = new Parse.Query("Task");

    $('#task-filter').val(localStorage.selectVal);

    switch (localStorage.selectVal) {
        case "all":
            var query = new Parse.Query("Task");
            break;
        case "bynameascending":
            query.ascending("taskName");
            break;
        case "bydeadline":
            query.ascending("taskDeadline");
            break;
        case "completed":
            query.equalTo("done", true);
            break;
        case "overdue":
            query.equalTo("done", false);
            query.lessThan("taskDeadline", date);
            break;
    }

    query.equalTo("assignedTo", user);

    query.find({
        //for if we want to use a href to do it
        success: function (results) {

            for (var i = 0; i < results.length; i++) {
                var task = results[i];
                var name = task.get("taskName");
                var id = task.id;
                var project = task.get("projectPointer")
                var description = task.get("taskDescription");
                var deadline = task.get("taskDeadline");
                ratings = task.get("totalRatings");
                var totalRatings = 0;

                if (ratings == null) {
                    totalRatings = 0;
                }
                else {
                    for (var j = 0; j < ratings.length; j++) {
                        totalRatings = (totalRatings + ratings[j].rating);
                    }
                    totalRatings = totalRatings / ratings.length;
                    totalRatings = totalRatings.toFixed(2);
                }

                $('#user-tasks').append(

                    //CREATE ITEM[i] HEADER
                    $('<tr>')
                    .addClass('panel panel-default clickable hr') //hr class is super important in css
                    .attr("data-target", id)
                    .attr('data-toggle', 'collapse')
                    .attr("id", "accordion" + id)
                    .append(

                        //TASK TITLE
                        $('<td>')
                            .addClass('panel panel-heading')
                            .append(
                                $('<h4>')
                                .append(
                                        $('<a>')
                                        .attr("data-toggle", "collapse")
                                        .attr("data-parent", "#accordion" + id)
                                        .attr('href', '#' + id)
                                        .append(name))))
                    .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<h4>')
                            .html(totalRatings)))
                    .append(
                    $('<td>')
                        .addClass('panel panel-heading')
                        .append(
                            $('<h4>')
                            .html((deadline.getMonth() + 1) + "/" + deadline.getDate() + "/" + deadline.getFullYear())))
                    .append(
                        $('<td>')
                            .addClass('panel panel-heading')
                            .append(
                                $('<h4>')
                                .html(project.get("projectName"))))
                    .append(
                        //CREATE BUTTONS 
                        $('<td>')
                                .addClass('panel panel-heading block')
                                .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-danger delete-task" data-toggle="tooltip" title="Delete Task"><span class="glyphicon glyphicon-trash"></span></a>'))
                                .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-info edit-task"><span class="glyphicon glyphicon-pencil" data-toggle="tooltip" title="Edit Task"></span></a>'))
                                .append($('<a href="#" data-tid="' + id + '" class="btn btn-default btn-sm btn-success complete-task"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="Mark Completed"></span></a>'))
                        ));

                //CREATE ITEM[i] COLLAPSIBLE CONTENT
                $('#user-tasks').append(
                    $('<tr>')
                    .attr("id", id)
                    .addClass('panel-collapse collapse')
                    .append(
                    $('<td>')
                        .attr("colspan", 3)
                        .addClass('panel panel-body')
                        .append(
                            $('<p>')
                            .addClass('item-header')
                            .append("Description:"))
                            .append(
                            $('<p>')
                            .append(description))
                            .append(
                            $('<p>')
                            .addClass('item-header'))));


                //Check if Task is Done
                if (results[i].get("done")) {
                    if (i == 0) {
                        $('#user-tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                    else {
                        $('#user-tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-success');
                    }
                }

                //Check if Task is Overdue

                if (deadline < date) {
                    if (i == 0) {
                        $('#user-tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-danger');
                    }
                    else {
                        $('#user-tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-danger');
                    }
                }

                //Check if Task is due soon (within 2 days)
                var date2 = new Date(date).setDate(date.getDate() + 2);
                if (deadline > date && deadline < date2) {
                    if (i == 0) {
                        $('#user-tasks tr:eq(' + (i) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-warning');
                        console.log("SHITS CLOSE DAWG");
                    }
                    else {
                        $('#user-tasks tr:eq(' + (i * 2) + ')')
                            .removeClass("panel-heading")
                            .addClass('panel-warning');
                    }
                }

                // OH GOD WHAT IF BOTH?

            }// END FOR LOOP
        }
    });
}

function getNotifications() {
    $('#notification-area').empty();
    var nQuery = new Parse.Query("Notification");
    nQuery.equalTo('hasBeenRead', false);
    nQuery.find({
        success: function (results) {
            for (i = 0; i < results.length; i++) {
                var nid = results[i].id;
                $('#notification-area').append($('<p>')
                    .attr('id', nid)
                    .html(results[i].get("content"))
                    .append(
                    $('<button>')
                .attr('type', "button")
                .addClass('btn btn-primary btn-sm')
                .addClass('dismissNotification')
                .attr('data-nid', nid)
                .html('Dismiss')
                ));
                //TODO - Put these notifications somewhere in the page,. attach their ID so that they can pass the value to deleteNotification
            }
        },
        error: function (results, error) {
            console.log("Error pulling Notifications!");
        }
    });
    $('#notification-modal').modal('toggle');
}

function deleteNotification() {

}
